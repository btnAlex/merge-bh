const express = require('express')
const app = express()
const port = 3000

const data = {}

const users = {
    admin: {
        login: 'admin',
        email: '',
        password: 'qr',
    }
}

app.use(express.json())

app.post('/sign-up', (req, res, ) => {
    const {login, email, password} = req.body
    if(!login) {
        throw new Error('Необходимо указать логин')
    }
    if(!email) {
        throw new Error('Необходимо указать E-mail')
    }
    if(!password) {
        throw new Error('Необходимо указать пароль')
    }
    if(users[login]) {
        throw new Error('Пользователь с таким именем уже существует')
    }
    users[login] = {login, email, password}
    res.send(
        users
    )
})

app.post('/sign-in', (req, res,) => {
    const {login, password} = req.body
    const user = users[login]
    if(!user) {
        throw new Error('Имя пользователя неверное')
    }
    if(user.password != password) {
        throw new Error('Пароль неверный')
    }
    res.send(
        `Вы прошли аутентификацию`
    )
})

app.post('/', (req, res) => {
    console.log(req.body)
    res.send(
        ''
    )
})

app.get('/', (req, res) => {
  res.send(`
  <h1>Data:</h1>
  <pre>${JSON.stringify(data, null, 4)}</pre>
  `)
})

app.get('/set/:key/:value', (req, res) => {
    console.log(req.params)
    data[req.params.key] = req.params.value
    res.send(JSON.stringify(data))
})

app.get('/sign-in', (req, res) => {
  res.send('You are Signed In')
})

app.use(function (err, req, res, next) {
    console.error(err.stack)
    res.status(500).send({error: err.message})
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

